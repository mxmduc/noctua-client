import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InscriptionComponent} from './client/inscription/inscription.component';
import {ConnexionComponent} from './client/connexion/connexion.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ProductListComponent} from './shop/product-list/product-list.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},

  {path: 'shop', component: ProductListComponent},
  {path: 'inscription', component: InscriptionComponent},
  {path: 'connexion', component: ConnexionComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
