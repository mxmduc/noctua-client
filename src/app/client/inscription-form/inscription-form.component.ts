import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-inscription-form',
  templateUrl: './inscription-form.component.html',
  styleUrls: ['./inscription-form.component.scss']
})
export class InscriptionFormComponent implements OnInit {
  @Input()
  public title = 'machin';

  registerForm: FormGroup;

  constructor(private fb: FormBuilder, public http: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      email: ['', Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')],
      password: ['', Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]).{8,64})')],
      passwordCheck: ['', Validators.minLength(2)],
      name: ['', Validators.minLength(2)],
      firstname: ['', Validators.minLength(2)],
      phone: ['', Validators.pattern('^(0|\\+33)[1-9]([-. ]?[0-9]{2}){4}$')],
      adress: ['', Validators.minLength(2)],
      postalCode: ['', Validators.pattern('^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$')],
      city: ['', Validators.minLength(2)],
    });
  }

  register(): void {
    // console.log('Données du formulaire...', this.registerForm.value);
    this.http.post(environment.API_URL + '/inscription', this.registerForm.value)
      .subscribe((res: any) => {
        console.log(res);
        if (res.status === 'Account created') {
          this.router.navigateByUrl('/connexion');
        }
      }, err => {
        console.dir(err);
      });
  }

}
