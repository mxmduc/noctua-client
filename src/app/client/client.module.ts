import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { InscriptionComponent } from './inscription/inscription.component';
import { InscriptionFormComponent } from './inscription-form/inscription-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { ConnexionComponent } from './connexion/connexion.component';
import { ConnexionFormComponent } from './connexion-form/connexion-form.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {HttpClient} from '@angular/common/http';



@NgModule({
  declarations: [
    InscriptionComponent,
    InscriptionFormComponent,
    ConnexionComponent,
    ConnexionFormComponent,
    DashboardComponent,
  ],
  exports: [
    InscriptionFormComponent,
    InscriptionComponent,
    ConnexionComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClient
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ClientModule { }
