import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AppModule} from '../app.module';
import {ProductListComponent} from './product-list/product-list.component';


@NgModule({
  declarations: [
    ProductListComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    AppModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShopModule {
}
