import { Component, OnInit } from '@angular/core';
import {products} from '../products';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  products = products.sort((a, b) => a.price - b.price);
  // tslint:disable-next-line:variable-name
  private plubishable_key: string;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  rent(product): void{
    this.http.post(environment.API_URL + '/product', product)
      .subscribe((res: any) => {
        console.log(res);
        this.plubishable_key = res.public_key;
      });
  }

}
