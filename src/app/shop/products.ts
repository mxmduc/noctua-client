export const products = [
  {
    name: 'Creeper',
    game: 'Minecraft',
    price: 3,
    description: 'Idéal pour débuter son aventure',
    img: 'https://zebrexy.fr/img/crepper/Modelisation-3D-creeper-minecraft.jpg',
    spec: {
      ram: 2,
      process: 'Intel Core 2,6 GHz',
      players: 4,
      stockage: 30,
      database: 1
    }
  },
  {
    name: 'Zombie',
    game: 'Minecraft',
    price: 699,
    description: 'Pour partager des moments de jeux entre amis',
    img: 'https://cs2.gtaall.eu/screenshots/d4861/2020-06/original/e301cfd5885ded1de3eae81ab02a2fd99fd05d1f/806867-gta-sa-2020-06-07-14-50-57-19-result.jpg',
    spec: {
      ram: 2,
      process: 'Intel Core 2,6 GHz',
      players: 4,
      stockage: 30,
      database: 1
    }
  },
  {
    name: 'Enderman',
    game: 'Minecraft',
    price: 299,
    description: 'Pour jouer sur des modpacks gourmands',
    img: 'https://d2skuhm0vrry40.cloudfront.net/2020/articles/2020-05-22-16-02/minecraft-dungeons-enderman-strategy-7029-1590159764852.jpg/EG11/resize/1200x-1/minecraft-dungeons-enderman-strategy-7029-1590159764852.jpg',
    spec: {
      ram: 2,
      process: 'Intel Core 2,6 GHz',
      players: 4,
      stockage: 30,
      database: 1
    }
  },
  {
    name: 'Enderman',
    game: 'Minecraft',
    price: 299,
    description: 'Pour jouer sur des modpacks gourmands',
    img: 'https://d2skuhm0vrry40.cloudfront.net/2020/articles/2020-05-22-16-02/minecraft-dungeons-enderman-strategy-7029-1590159764852.jpg/EG11/resize/1200x-1/minecraft-dungeons-enderman-strategy-7029-1590159764852.jpg',
    spec: {
      ram: 2,
      process: 'Intel Core 2,6 GHz',
      players: 4,
      stockage: 30,
      database: 1
    }
  }
];
